api = 2
core = 7.x

; NodeStream core
projects[ns_core][type] = module
projects[ns_core][version] = 7.x-2.x
projects[ns_core][download][type] = git
projects[ns_core][download][branch] = 7.x-2.x
projects[ns_core][subdir] = contrib

; Panopoly core
projects[panopoly_core][version] = 1.0-rc2  
projects[panopoly_core][subdir] = contrib

; All the happy children of the family

projects[panopoly_magic][version] = 1.0-rc2  
projects[panopoly_magic][subdir] = contrib

projects[panopoly_admin][version] = 1.0-rc2  
projects[panopoly_admin][subdir] = contrib

projects[panopoly_pages][version] = 1.0-rc2  
projects[panopoly_pages][subdir] = contrib

projects[panopoly_images][version] = 1.0-rc2
projects[panopoly_images][subdir] = contrib

projects[panopoly_widgets][version] = 1.0-rc2
projects[panopoly_widgets][subdir] = contrib

projects[panopoly_wysiwyg][version] = 1.0-rc2
projects[panopoly_wysiwyg][subdir] = contrib

projects[ns_prod_enterprise][type] = module
projects[ns_prod_enterprise][version] = 7.x-2.x
projects[ns_prod_enterprise][download][type] = git
projects[ns_prod_enterprise][download][branch] = 7.x-2.x
projects[ns_prod_enterprise][subdir] = contrib

; Responsive bartik to make it all pretty
projects[panopoly_theme][version] = 1.0-rc2
projects[panopoly_theme][subdir] = contrib